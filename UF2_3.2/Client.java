/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foo;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Query;

@Entity
@Table (name = "client")

public class Client implements Serializable{
    @Id
    @Column (name = "dni")
    int dni;
    @Column (name = "nom")
    String nom;
    @OneToMany
    @Column (name = "premium")
    boolean premium;
    
    public Client () {}
    
    public void newClient (){
        Scanner scDni = new Scanner(System.in);
        Scanner scNom = new Scanner(System.in);
        Scanner scPremium = new Scanner(System.in);

        System.out.print("Introdueix el DNI del client");
        this.dni = scDni.nextInt();
        System.out.print("Introdueix el Nom del client");
        this.nom = scNom.nextLine();
        System.out.print("Es Premium S/Y");
        String premium = scPremium.nextLine().toLowerCase();
        if (premium.equals("y")){
            this.premium = true;
        }else{
            this.premium = false;
        }
        
    }

    @Override
    public String toString() {
        return "Client{" + "dni=" + dni + ", nom=" + nom + ", premium=" + premium + '}';
    }
}