/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author PAPILLON
 */
@Entity
@Table (name = "comanda")
public class Comanda {
    @Id @GeneratedValue
    @Column (name = "num_comanda")
    int num_comnada;
    @Column (name = "preu_total")
    int preu_total;
    @Column (name = "data")
    Date data;
    @JoinColumn (name = "dni_client")
    int dni_client;
    public Comanda () {}
     
    public void newComanda(int dni) throws ParseException{
        this.dni_client = dni;
        Scanner scNum = new Scanner(System.in);
        Scanner scPreu = new Scanner(System.in);
        Scanner scData = new Scanner(System.in);
        
        System.out.print("Introdueix numero de comanda ");
        this.num_comnada = scNum.nextInt();
        System.out.print("Introdueix preu de la comanda ");
        this.preu_total = scPreu.nextInt();
        System.out.print("Introdueix nla data de la comanda DD-MM-YYYY");
        String data = scData.nextLine();
        Date dataFoo = new SimpleDateFormat("dd-MM-yyyy").parse(data);
        this.data = dataFoo;
    }

    @Override
    public String toString() {
        return "Comanda{" + "num_comnada=" + num_comnada + ", preu_total=" + preu_total + ", data=" + data + ", dni_client=" + dni_client + '}';
    }
}
