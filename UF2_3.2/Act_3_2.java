/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foo;

import java.text.ParseException;
import java.util.List;
import java.util.Scanner;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author PAPILLON
 */
public class Act_3_2 {
    
    public void eliminarClient (){
        Client cliente = new Client();
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            
            Scanner dniScanner = new Scanner(System.in);
        
            System.out.print("Iintrodueix el dni del Client que volem eliminar");
            String dni = dniScanner.nextLine();
        
            Query query = session.createQuery("delete from Clientes where dni = " + dni);
            query.executeUpdate();
            session.getTransaction().commit();
        
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);

        }
    }
    
    public void cercaClient (String iniciBusca){
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            
            Query q = session.createQuery("from Client where nom like " + iniciBusca + "%");
            
            List<Client> listClients = q.list();
            for (Client c : listClients){
                System.out.print(c);
            }
        
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);

        }
    }
    
    public void nouClient (){
        
        Client c = new Client();
        c.newClient();
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            session.beginTransaction();
            session .save(c);
            session.getTransaction().commit();

        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);

        }
    }
    
    public void nouCommanda () throws ParseException{
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            
            Query q = session.createQuery("from Clients");
            List<Client> listClients = q.list();
            for (int i = 0; i < listClients.size(); i++) {
                Client c = listClients.get(i);
                System.out.print(i + " " + c);
            }
            System.out.print("Selecciona el numero d'un client");
            Scanner scOpcio = new Scanner(System.in);
            int opcio = scOpcio.nextInt();
            Client c = listClients.get(opcio);
            Comanda comanda = new Comanda();
            comanda.newComanda(c.dni);
            
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);

        }
    }
    
    public void comandesClient () throws ParseException {
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            
            Query qCl = session.createQuery("from Clients");
            List<Client> listClients = qCl.list();
            for (int i = 0; i < listClients.size(); i++) {
                Client c = listClients.get(i);
                System.out.print(i + " " + c);
            }
            System.out.print("Selecciona el numero d'un client");
            Scanner scOpcio = new Scanner(System.in);
            int opcio = scOpcio.nextInt();
            Client c = listClients.get(opcio);
            
            System.out.print("Lliste de comandes de " + c.nom);            
            Query qCm = session.createQuery("form Comanda where dni_client = " + c.dni);
            List<Comanda> listComanda = qCm.list();
            for (Comanda com : listComanda){
                System.out.print(com);
            }  
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);

        }
    }
    
    public void resumClientsComanda () {
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            
            Query q = session.createQuery("");
            System.out.print(q);
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);

        }
    }
}
