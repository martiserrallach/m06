import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws SQLException {
        MySql mySql = new MySql();

        int num = menu();
        switch (num){
            case 1:
                mySql.creacioTaulaClients();
                mySql.creacioTaulaComandes();
                break;
            case 2:
                mySql.recuperacioClients();
                break;
            case 3:
                mySql.emmagatzamentDeDadesBBDD();
                break;
            case 4:
                mySql.altaClient();
                break;
            case 5:
                mySql.altaComanda();
                break;
            case 6:
                mySql.comandesClient();
                break;
        }

    }

    static int menu (){
        Scanner numeroMenu = new Scanner(System.in);
        System.out.println("Selecciona una de les opcions seguents:\n" +
                "1 - Creacio taules\n" +
                "2 - Recuperacio de les Dades\n" +
                "3 - Emmegatzematge de la BBDD\n" +
                "4 - Alta nou client\n" +
                "5 - Alta nova comanda\n" +
                "6 - Mostre per pantalla les comandes dun client");
        int num = numeroMenu.nextInt();
        return num;
    }
}
