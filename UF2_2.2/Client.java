import java.util.ArrayList;

public class Client {
    int dni;
    String nom;
    boolean premium;
    ArrayList<Comandes> comandes = new ArrayList<>();

    public Client(int dni, String nom, boolean premium) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premium;
    }

    public Client() {
    }

    @Override
    public String toString() {
        return "Client{" +
                "dni=" + dni +
                ", nom='" + nom + '\'' +
                ", premium=" + premium +
                ", comandes=" + comandes +
                '}';
    }
}
