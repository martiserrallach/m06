import java.time.LocalDate;
import java.util.ArrayList;

public class Comandes {
    int num_comandes;
    double preu_total;
    LocalDate date;
    int dni_client;

    public Comandes(int num_comandes, double preu_total, LocalDate date, int dni_client) {
        this.num_comandes = num_comandes;
        this.preu_total = preu_total;
        this.date = date;
        this.dni_client = dni_client;
    }

    public Comandes() {

    }

    @Override
    public String toString() {
        return "Comandes{" +
                "num_comandes=" + num_comandes +
                ", preu_total=" + preu_total +
                ", date=" + date +
                ", dni_client=" + dni_client +
                '}';
    }
}
