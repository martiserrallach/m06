DROP PROCEDURE if exists crea_resum_facturacio;
delimiter // 
CREATE PROCEDURE crea_resum_facturacio(p_mes int, p_any int) BEGIN
DECLARE v_dni_client, v_preu_total int;
DECLARE acaba INT DEFAULT FALSE;
DECLARE c_comanda CURSOR FOR SELECT dni_client, SUM(preu_total) AS preu_total FROM COMANDES WHERE month(data) = p_mes AND year(data) = p_any group by dni_client;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET acaba = TRUE; 
OPEN c_comanda; 
read_loop: LOOP 
	FETCH c_comanda INTO v_dni_client, v_preu_total; 
	IF acaba THEN
        LEAVE read_loop;
    END IF;
		INSERT RESUM_FACTURACIO (mes, any, dni_client, preu_total) values (p_mes, p_any, v_dni_client, v_preu_total);
END LOOP;
CLOSE c_comanda;
END;
//    