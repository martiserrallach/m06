import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MySql {
    String url = "jdbc:mysql://localhost/lamevabbdd";
    String user = "guest";
    String paswd = "guest";
    Connection connection = null;

    public MySql() throws SQLException {

    }

    public void connectionMySql() throws SQLException {
        connection = DriverManager.getConnection(url,user,paswd);
    }

    public void creacioTaulaClients() throws SQLException {
        connectionMySql();
        String taulaSQL = "CREATE TABLE CLIENTS("
                + "dni INT (8), "
                + "nom VARCHAR (50), "
                + "premium CHAR, "
                + "CONSTRAINT PK_CLIENT_dni PRIMARY KEY(dni));";
        Statement statement = connection.createStatement();
        statement.execute(taulaSQL);
    }

    public void dropClients() throws SQLException {
        connectionMySql();
        String taulaSQL = "DROP TABLE CLIENTS;";
        Statement statement = connection.createStatement();
        statement.execute(taulaSQL);
    }

    public void creacioTaulaComandes() throws SQLException {
        connectionMySql();
        String taulaSQL = "CREATE TABLE COMANDES("
                + "num_comandes INT , "
                + "preu_total FLOAT (2), "
                + "data DATE, "
                + "dni_client INT (8), "
                + "CONSTRAINT PK_COMANDES_num_comandes PRIMARY KEY(num_comandes), "
                + "CONSTRAINT FK_COMANDES_dni_client FOREIGN KEY (dni_client) REFERENCES CLIENTS(dni));";
        Statement statement = connection.createStatement();
        statement.execute(taulaSQL);
    }

    public void dropComandes() throws SQLException {
        connectionMySql();
        String taulaSQL = "DROP TABLE COMANDES;";
        Statement statement = connection.createStatement();
        statement.execute(taulaSQL);
    }

    public ArrayList<Client> recuperacioClients() throws SQLException {
        connectionMySql();

        String sentenciaSQLClients = "select DNI,NOM,PREMIUM from CLIENTS;";
        Statement statementCl = this.connection.createStatement();
        ResultSet rsCl = statementCl.executeQuery(sentenciaSQLClients);

        ArrayList<Client> llista = new ArrayList<>();
        while (rsCl.next()) {
            Client client = new Client();
            client.dni = rsCl.getInt("DNI");
            client.nom = rsCl.getString("NOM");
            client.premium = (rsCl.getString("PREMIUM").equals("y"));

            String sentenciaSQLComandes = "SELECT num_comandes,preu_total,data,dni_client from COMANDES WHERE dni_client = " + client.dni + ";" ;
            Statement statementCom = this.connection.createStatement();
            ResultSet rsCom = statementCom.executeQuery(sentenciaSQLComandes);

            while (rsCom.next()) {
                Comandes comanda = new Comandes();
                comanda.num_comandes = rsCom.getInt("num_comandes");
                comanda.dni_client = rsCom.getInt("dni_client");
                comanda.preu_total = rsCom.getFloat("preu_total");
                comanda.date = rsCom.getDate("data").toLocalDate();
                client.comandes.add(comanda);
            }
            llista.add(client);
        }
        System.out.println(llista);
        return llista;
    }

    public void emmagatzamentDeDadesBBDD () throws SQLException {
        ArrayList<Client> clients = recuperacioClients();

        dropComandes();
        dropClients();
        creacioTaulaClients();
        creacioTaulaComandes();

        for (int i = 0; i < clients.size(); i++) {
            Client client = clients.get(i);
            insertClient(client);
            for (int j = 0; j < client.comandes.size(); j++) {
                Comandes comandes = client.comandes.get(j);
                insertComanda(comandes);
            }
        }
    }

    public void insertClient (Client client) throws SQLException {
        connectionMySql();
        String sentenciaSQL = "insert into CLIENTS (dni,nom,premium) values (?,?,?)";
        PreparedStatement sentenciaPreparada = connection.prepareStatement(sentenciaSQL);
        sentenciaPreparada.setInt(1,client.dni);
        sentenciaPreparada.setString(2,client.nom);
        sentenciaPreparada.setBoolean(3,client.premium);
        sentenciaPreparada.executeUpdate();
    }

    public void insertComanda (Comandes comanda) throws SQLException {
        connectionMySql();
        String sentenciaSQL = "insert into COMANDES (num_comandes,preu_total,data,dni_client) values (?,?,?,?)";
        PreparedStatement sentenciaPreparada = connection.prepareStatement(sentenciaSQL);
        sentenciaPreparada.setInt(1,comanda.num_comandes);
        sentenciaPreparada.setDouble(2,comanda.preu_total);
        sentenciaPreparada.setDate(3, Date.valueOf(comanda.date));
        sentenciaPreparada.setInt(4,comanda.dni_client);
        sentenciaPreparada.executeUpdate();
    }

    public void altaClient () throws SQLException {
        Client client = new Client();
        ArrayList<Client> clients = recuperacioClients();
        Scanner nifClient = new Scanner(System.in);
        Scanner nomClient = new Scanner(System.in);
        Scanner premiumClient = new Scanner(System.in);

        System.out.println("Introdueix el nif (sense lletra) del nou client: ");
        client.dni = nifClient.nextInt();
        System.out.println("Introdueix el nom del nou client");
        client.nom = nomClient.nextLine();
        System.out.println("Es premium: ture/false");
        client.premium = premiumClient.nextBoolean();

        clients.add(client);
        insertClient(client);
    }

    public void altaComanda () throws SQLException {
        ArrayList<Client> clients = recuperacioClients();
        Client client = new Client();
        Comandes comande = new Comandes();
        int num = 1;
        Scanner numClient = new Scanner(System.in);

        for (int i = 0; i < clients.size(); i++) {
            client = clients.get(i);
            System.out.println(num + " -" + client.dni + ": " + client.nom);
            num ++;
        }
        System.out.println("Selecciona el Client");
        int numOption = numClient.nextInt();

        client = clients.get(numOption-1);
        System.out.println("Crearem una nova comanda per " + client.nom);
        Scanner numComanda = new Scanner(System.in);
        Scanner preuComanda = new Scanner(System.in);
        Scanner localDate = new Scanner(System.in);
        Scanner dniClientComand = new Scanner(System.in);
        System.out.println("Numero de comanda:");
        comande.num_comandes = numComanda.nextInt();
        System.out.println("Preu de la comanda: ");
        comande.preu_total = preuComanda.nextDouble();
        System.out.println("Data de la Comanda: ");
        comande.date = LocalDate.parse(localDate.nextLine());
        System.out.println("DNI Client: ");
        comande.dni_client = dniClientComand.nextInt();
        client.comandes.add(comande);
        clients.add(client);
        insertClient(client);
        insertComanda(comande);

        System.out.println(clients);
    }

    public void comandesClient () throws SQLException {
        connectionMySql();
        ArrayList<Client> clients = recuperacioClients();
        Client client = new Client();

        Scanner nifClient = new Scanner(System.in);
        System.out.println("Introdueix el nif (Sense lletra) dun client: ");
        int dniclient = nifClient.nextInt();

        for (int i = 0; i < clients.size(); i++) {
            client = clients.get(i);
            if (dniclient == client.dni){
                client.comandes.toString();
            }
        }
    }

    public void resumFacturacio () throws SQLException {
        connectionMySql();
        Scanner mesScanner = new Scanner(System.in);
        Scanner anyScanner = new Scanner(System.in);

        System.out.println("Introdueix el mes que vols buscar en numero del 1 al 12");
        int mesFacturacio = mesScanner.nextInt();
        if (mesFacturacio <= 0 && mesFacturacio >= 13){
            System.out.println("Mes no valid introdueix el numero de mes valid tenint en compte que el mes 1 es gener i el 12 desembre");
            System.exit(0);
        }

        System.out.println("Introdueix l'any que vols resumir");
        int anyFacturacio = anyScanner.nextInt();
        if (Integer.toString(anyFacturacio).length() == 4 && anyFacturacio <= 1900){
            System.out.println("Any no valid introdueix l'any valid, recorda que ha de tenir quatre digits i desde ");
            System.exit(0);
        }

        String sentenciaSQL = "call crea_resum_facturacio(?,?)";
        PreparedStatement sentenciaPreparada = connection.prepareStatement(sentenciaSQL);
        sentenciaPreparada.setInt(1,mesFacturacio);
        sentenciaPreparada.setInt(2,anyFacturacio);
        sentenciaPreparada.executeUpdate();
    }

}
